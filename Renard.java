import java.util.*;
import java.lang.Math;

public class Renard extends Animaux{

  private Monde m;


  public Renard(Monde m){
    super(m);
    this.m = super.getMonde(); // un lapin doit connaitre le monde dans lequel il se trouve pour pouvoir se déplacer
    super.addAlimentation(new Lapin(m));
    super.addAlimentation(new LapinAffame(m, 0));
    super.setRapporteEnergie(8);
    super.addDestinationPossible(new Prairie());
    super.addDestinationPossible(new Plaine());
    super.setEnergieMax(20);
    super.setEnergie(15);
    super.setSeuilAffame(4);
  }

  public void jourSuivant(){
    super.jourSuivant();
    this.seDeplace();
  }
  public void seDeplace(){
    List<Lieu> cheminAParcourir = null;
    cheminAParcourir = this.m.getCheminVersNourriture(this);
    if(cheminAParcourir!=null){
      Lieu interaction=this.m.getLieuActuel(this);

      for(Lieu l : cheminAParcourir){
        Lieu lPrec=this.m.getLieuActuel(this);
        lPrec.removeHabitant(this);
        l.addHabitant(this);
        super.addEnergie(-1);
        this.m.afficheSwing();
        interaction=l;
      
      }
      if(interaction!=null){
        List<Etre> listeTampon = new ArrayList<>(interaction.getListHabitant());
        for(Etre e : listeTampon){
          if(e!=this){
            if(super.canEat(e)){
              if(super.getEnergie()<super.getEnergieMax()-4){
                super.eat(e);
                System.out.println(this.getNom()+" a graille une bonne "+e.toString());
              }
            }
          }
        }
      }
    }
  }


  public Etre changementEtat(){
    return super.changementEtat();
  }



  public String toString(){
    String res = "🦊";
    return res;
  }
}
