import java.util.*;
import java.lang.Math;
public class Lapin extends Animaux{

  private Monde m;


  public Lapin(Monde m){
    super(m);
    this.m = super.getMonde(); // un lapin doit connaitre le monde dans lequel il se trouve pour pouvoir se déplacer
    super.addAlimentation(new Carotte());
    super.addAlimentation(new CarottePerime());
    super.setRapporteEnergie(5);
    super.addDestinationPossible(new Prairie());
    super.addDestinationPossible(new Plaine());
    super.setEnergieMax(10);
    super.setEnergie(8);
    super.setSeuilAffame(3);
  }




  public void jourSuivant(){
    super.jourSuivant();
    this.seDeplace();
  }

  public void seDeplace(){
    // un lapin qui n'est pas affame cherchera tout d'abord un lapin du sexe opposé pour se reproduire, et s'il y en a plusieurs, il choisira le plus proche possible
    // sinon il se dirigera vers le lieu le plus proche qui possède des carottes (peu importe le nombre de carottes)
    List<Lieu> cheminAParcourir = null;
    Boolean reprod = false;
    if(!(super.isInCooldown())){
      cheminAParcourir = this.m.getCheminVersPretendant(this);
      reprod=true;
    }
    if(cheminAParcourir==null){
      cheminAParcourir = this.m.getCheminVersNourriture(this);
      reprod=false;

    }


    if(cheminAParcourir!=null){
      Lieu interaction=this.m.getLieuActuel(this);

      for(Lieu l : cheminAParcourir){
        Lieu lPrec=this.m.getLieuActuel(this);
        lPrec.removeHabitant(this);
        l.addHabitant(this);
        super.addEnergie(-1);
        this.m.afficheSwing();
        interaction=l;
    
      }

      System.out.println(interaction);
        if(interaction!=null){
          List<Etre> listeTampon = new ArrayList<>(interaction.getListHabitant());
          for(Etre e : listeTampon){
            if(e!=this){

              if(reprod && !super.isInCooldown()){
                if(e.getClass()==this.getClass()){
                  Lapin l = (Lapin) e;
                  if(!(l.isInCooldown()) && l.getMale()!=this.getMale()){
                    super.setCooldown(10);
                    l.setCooldown(10);
                    interaction.addHabitant(new Lapin(m));
                  }
                }
              }
              else{
                if(super.canEat(e)){
                  if(super.getEnergie()<super.getEnergieMax()-2){
                    super.eat(e);
                  }
                }
              }
            }

            // this.m.affiche();
          }
        }
      }
    }


  public Etre changementEtat(){
    if(super.is_affame()){
      return new LapinAffame(this.m, super.getEnergie());
    }
    return super.changementEtat();
  }



  public String toString(){
    String res = "🐰";
    return res;
  }
}
