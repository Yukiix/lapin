import javax.swing.*;
import java.awt.event.*;

public class JourSuivantButton extends AbstractAction {
	private ExecutableSwing fenetre;
  private Monde m;

	public JourSuivantButton(ExecutableSwing fenetre, String texte, Monde m){
		super(texte);
    this.m = m;
		this.fenetre = fenetre;
	}

	public void actionPerformed(ActionEvent e) {
    if(this.fenetre.getJour()%10==9){
      this.m.getLieu(0,3).addHabitant(new Renard(m));
    }
		// this.fenetre.majAffichageNoButton();
		// try
		// {
		// 		Thread.sleep(1000);
		// }
		// catch(InterruptedException ex)
		// {
		// 		Thread.currentThread().interrupt();
		// }
    m.jourSuivant();
		this.fenetre.majAffichage();

    m.affiche();
    this.fenetre.setJour(this.fenetre.getJour()+1);
	}
}
