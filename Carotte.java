import java.util.*;

public class Carotte extends Vegetaux{

  private Boolean perime;

  public Carotte(){
    super();
    this.perime=false;
    super.setRapporteEnergie(3);
  }

  public void jourSuivant(){
    super.jourSuivant();
    if(super.getAge()>3){
      this.perime=true;
    }
  }

  public Boolean isPerime(){
    return this.perime;
  }

  public String toString(){
    return "🥕";
  }

  public Etre changementEtat(){
    Etre res=null;
    if(this.isPerime()){
      return new CarottePerime();
    }
    return res;
  }

}
