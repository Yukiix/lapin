import java.util.*;
import java.awt.Color;


public class Prairie extends Lieu{


  public Prairie(Integer posX, Integer posY){
    super(posX, posY, "Prairie");
    super.setColor(new Color(84, 185, 1));
  }
  public Prairie(){
    super(0, 0, "Prairie");
    super.setColor(new Color(84, 185, 1));
  }

  public void jourSuivant(){
    super.addHabitant(new Carotte());
    super.jourSuivant();
  }

  public Integer getNbCarottes(){
    Integer res=0;
    Carotte c=new Carotte();
    for(Etre e : super.getListHabitant()){
      if(e.getClass() == c.getClass()){
        Carotte f = (Carotte) e;
        if(!f.isPerime()){
          res++;
        }
      }
    }
    return res;
  }


}
