public class AnimalMort extends Animaux{

  public AnimalMort(Monde m){
    super(m);
    super.setRapporteEnergie(3);
  }

  public void jourSuivant(){
    // chaque jour un animalMort rapporte un peu moins d'energie
    super.setRapporteEnergie(super.getRapporteEnergie()-1);
  }

  public Etre changementEtat(){
    // il disparait s'il ne rapporte plus du tout d'énergie
    if(super.getRapporteEnergie()<=0){
      super.set_dead();
    }
    return null;
  }

  public String toString(){
    return "🍖";
  }
}
