import java.util.*;

public class CarottePerime extends Vegetaux{


  public CarottePerime(){
    super.setRapporteEnergie(1);
  }

  public String toString(){
    return "🍲";
  }

  public Etre changementEtat(){
    if(super.getAge()>0){
      super.set_dead();
    }
    return null;
  }

}
