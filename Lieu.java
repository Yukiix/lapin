import java.util.*;
import java.awt.Color;

public abstract class Lieu{

  private Integer posX;
  private Integer posY;
  private String name;
  private List<Etre> lHabitant;
  private Color color;


  public Lieu(Integer posX, Integer posY, String nom){
    this.posX=posX;
    this.posY=posY;
    this.name=nom;
    this.lHabitant = new ArrayList<>();
    this.color=new Color(0,0,0);
  }
  public void setColor(Color color){
    this.color=color;
  }
  public Color getColor(){
    System.out.println(this.color);
    return this.color;
  }

  public List<Etre> getListHabitant(){
    return this.lHabitant;
  }


  public void addHabitant(Etre e){
    this.lHabitant.add(e);
  }

  public void removeHabitant(Etre e){
    this.lHabitant.remove(e);
  }

  public String toString(){
    ArrayList<Etre> list = new ArrayList<>();
    String res="";
    for(Etre e : this.lHabitant){
      Boolean notInList=true;
      for(Etre e1 : list){
        if(e.getClass()==e1.getClass()){
          notInList=false;
        }
      }
      if(notInList){
        list.add(e);
        res+=e.toString()+": "+this.getNbEspeces(e)+" ";
      }
    }
    res+="";
    return res;
  }

  public Integer getX(){
    return this.posX;
  }

  public Integer getY(){
    return this.posY;
  }

  public Integer getNbEspeces(Etre e){
    Integer res=0;
    for(Etre e1 : this.lHabitant){
      if(e.getClass()==e1.getClass()){
        res++;
      }
    }
    return res;
  }

  public void jourSuivant(){
    ArrayList<Etre> listeTampon = new ArrayList<>(this.lHabitant);
    for(Etre e : listeTampon){
      e.jourSuivant();
      Etre f = e.changementEtat();
      if(f!=null){
        this.lHabitant.remove(e);
        this.lHabitant.add(f);
      }
      else{
        if(e.is_dead()){
          this.lHabitant.remove(e);
        }
      }
    }
  }


}
