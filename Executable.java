import java.util.*;
public class Executable{
  public static void main(String[] args){
    Monde m = new Monde();
    Prairie p1 = new Prairie(0,0);
    Plaine p2 = new Plaine(0,1);
    Prairie p3 = new Prairie(2,1);
    Plaine p5 = new Plaine(1,1);
    Prairie p4 = new Prairie(0,2);
    Plaine p6 = new Plaine(0,3);
    Prairie p7 = new Prairie(2,2);
    Prairie p8 = new Prairie(1,3);
    Plaine p9 = new Plaine(2,3);
    Lapin l = new Lapin(m);
    Lapin l2 = new Lapin(m);

    p1.addHabitant(l);
    p7.addHabitant(l2);
    m.addLieu(p1);
    m.addLieu(p2);
    m.addLieu(p3);
    m.addLieu(p4);
    m.addLieu(p5);
    m.addLieu(p6);
    m.addLieu(p7);
    m.addLieu(p8);
    m.addLieu(p9);




    m.affiche();
    Integer jour=0;
    while(true){
      Scanner myObj = new Scanner(System.in);  // Create a Scanner object
      String userName = myObj.nextLine();  // Read user input
      if(jour%10==9){
        p6.addHabitant(new Renard(m));
      }
      m.jourSuivant();
      m.affiche();
      jour++;
    }
  }
}
