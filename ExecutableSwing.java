import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.applet.Applet;

public class ExecutableSwing extends JFrame {
  private Integer jour;
  private Monde m;
  private Affichage a;
  public Integer getJour(){
    return this.jour;
  }
  public void setJour(Integer i){
    this.jour=i;
  }

   public ExecutableSwing() {
      super("Le club Lapinou");

      // Generation du contenu
      Monde m = new Monde(this);
      Prairie p1 = new Prairie(0,0);
      Plaine p2 = new Plaine(0,1);
      Prairie p3 = new Prairie(2,1);
      Plaine p5 = new Plaine(1,1);
      Prairie p4 = new Prairie(0,2);
      Plaine p6 = new Plaine(0,3);
      Prairie p7 = new Prairie(2,2);
      Prairie p8 = new Prairie(1,3);
      Plaine p9 = new Plaine(2,3);
      Lapin l1 = new Lapin(m);
      Lapin l2 = new Lapin(m);

      p1.addHabitant(l1);
      p7.addHabitant(l2);
      m.addLieu(p1);
      m.addLieu(p2);
      m.addLieu(p3);
      m.addLieu(p4);
      m.addLieu(p5);
      m.addLieu(p6);
      m.addLieu(p7);
      m.addLieu(p8);
      m.addLieu(p9);


      m.affiche();
      this.jour=0;
      this.m=m;
      // Generation de la fenetre

      WindowListener l = new WindowAdapter() {
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      };
      JButton b =new JButton(new JourSuivantButton(this, "Jour suivant", m));
      Affichage affichage = new Affichage(b, m);
      this.a=affichage;
      super.getContentPane().add(this.a);
      addWindowListener(l);
      setSize(200,100);
      setVisible(true);
   }

   public static void main(String [] args){
      JFrame frame = new ExecutableSwing();
      JFrame f = frame;
      f.setSize(1000,500);
      f.setVisible(true);

   }

   public void majAffichage(){
     this.a.majAffichage();
     this.revalidate();
     this.repaint();
   }

   public void majAffichageNoButton(){
     this.a.majAffichageNoButton();
     this.revalidate();
     this.repaint();
   }
}
