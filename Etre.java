import java.util.*;

public abstract class Etre{


  private List<Lieu> peut_aller;
  private Integer rapporte_energie;
  private Integer age;
  public Boolean is_dead;

  public Etre(){
    this.peut_aller=new ArrayList<>();
    this.age=0;
    this.is_dead=false;
  }
  public void setRapporteEnergie(Integer r){
    this.rapporte_energie=r;
  }
  public void addDestinationPossible(Lieu l){
    this.peut_aller.add(l);
  }
  public List<Lieu> getDestinationPossible(){
    return this.peut_aller;
  }

  public void jourSuivant(){
    this.age+=1;
  }
  public Integer getAge(){
    return this.age;
  }
  public Integer getRapporteEnergie(){
    return this.rapporte_energie;
  }
  public void set_dead(){
    this.is_dead=true;
  }
  public Boolean is_dead(){
    return this.is_dead;
  }
  public Etre changementEtat(){
    return null;
  }

}
