import java.util.*;
import java.lang.Math;

public class LapinAffame extends Animaux{

  private Monde m;

  public LapinAffame(Monde m,Integer energie){
    super(m);
    this.m = super.getMonde(); // un lapin doit connaitre le monde dans lequel il se trouve pour pouvoir se déplacer
    super.addAlimentation(new Carotte());
    super.setRapporteEnergie(5);
    super.addDestinationPossible(new Prairie());
    super.addDestinationPossible(new Plaine());
    super.setEnergieMax(10);
    super.setEnergie(energie);
    super.setSeuilAffame(3);
  }
  public void jourSuivant(){
    super.jourSuivant();
    this.seDeplace();
  }
  public void seDeplace(){
    List<Lieu> cheminAParcourir = null;
    cheminAParcourir = this.m.getCheminVersNourriture(this);
    if(cheminAParcourir!=null){
      Lieu interaction=this.m.getLieuActuel(this);

      for(Lieu l : cheminAParcourir){
        Lieu lPrec=this.m.getLieuActuel(this);
        lPrec.removeHabitant(this);
        l.addHabitant(this);
        super.addEnergie(-1);
        this.m.afficheSwing();
        interaction=l;

      }
      if(interaction!=null){
        List<Etre> listeTampon = new ArrayList<>(interaction.getListHabitant());
        for(Etre e : listeTampon){
          if(e!=this){
            if(super.canEat(e)){
              if(super.getEnergie()<super.getEnergieMax()-2){
                super.eat(e);
                System.out.println(this.getNom()+" a graille une bonne "+e.toString());
              }
            }
          }
        }
      }
    }
  }

  public Etre changementEtat(){
    if(!(super.is_affame())){
      return new Lapin(this.m);
    }
    return super.changementEtat();
  }



  public String toString(){
    String res = "🐰🤤";
    return res;
  }
}
