import java.util.*;

public class Monde{

  private List<Lieu> lLieu;
  private Integer maxX;
  private Integer maxY;
  private Integer nombreMale=0;
  private Integer nombreFemelle=0;
  private List<String> nomMale;
  private List<String> nomFemelle;
  private ExecutableSwing fenetre;

  public Integer getMaxX(){
    return this.maxX;
  }
  public Integer getMaxY(){
    return this.maxY;
  }

  public Monde(){
    this.fenetre=null;
    this.lLieu = new ArrayList<>();
    this.maxX=0;
    this.maxY=0;
    this.nomMale  = Arrays.asList(
                "Bertrand",
                "Jacque",
                "Thierry",
                "Calogero",
                "Fabtherabbit",
                "Alpha",
                "Jamy",
                "Fred"
                );
    this.nomFemelle  = Arrays.asList(
                "Jennifer",
                "Martine",
                "Josianne",
                "Claudine",
                "Henriette",
                "Rillette",
                "Baladur",
                "Sabine",
                "Marcel"
                );
  }
  public Monde(ExecutableSwing e){
    this.fenetre=e;
    this.lLieu = new ArrayList<>();
    this.maxX=0;
    this.maxY=0;
    this.nomMale  = Arrays.asList(
                "Bertrand",
                "Jacque",
                "Thierry",
                "Calogero",
                "Fabtherabbit",
                "Alpha",
                "Jamy",
                "Fred"
                );
    this.nomFemelle  = Arrays.asList(
                "Jennifer",
                "Martine",
                "Josianne",
                "Claudine",
                "Henriette",
                "Rillette",
                "Baladur",
                "Sabine",
                "Marcel"
                );
  }
  public void afficheSwing(){
    System.out.println(this.fenetre);
    System.out.println("ceqfq");
    if(this.fenetre!=null){
      // this.fenetre.majAffichage();
    }

  }

  public String getNewNomMale(){
    String res = this.nomMale.get(this.nombreMale%this.nomMale.size())+" "+this.nombreMale/this.nomMale.size();
    this.nombreMale++;
    return res;
  }

  public String getNewNomFemelle(){
    String res = this.nomFemelle.get(this.nombreFemelle%this.nomFemelle.size())+" "+this.nombreFemelle/this.nomFemelle.size();
    this.nombreFemelle++;
    return res;
  }

  public List<Lieu> getListLieu(){
    return this.lLieu;
  }

  public void addLieu(Lieu l){
    this.lLieu.add(l);
    if(l.getX()>this.maxX){ // on met à jour la size du monde en fonction du nouveau Lieu
      this.maxX=l.getX();
    }
    if(l.getY()>this.maxY){
      this.maxY=l.getY();
    }
  }

  public void removeLieu(Lieu l){
    this.lLieu.remove(l);
  }

  public void affiche(){
    Integer compteurX=0;
    Integer compteurY=0;
    while(compteurX<=this.maxX){
      while(compteurY<=this.maxY){
        Lieu lieu = this.getLieu(compteurX, compteurY);
        if(lieu != null){
            System.out.print(lieu+" ");
        }
        else{
          System.out.print("null ");

        }
        compteurY++;
      }
      System.out.println("");
      compteurY=0;
      compteurX++;
    }
  }

  public String toString(){
    return this.lLieu.toString();
  }

  public Lieu getLieu(Integer posX, Integer posY){
    Lieu res = null;
    for(Lieu l : this.lLieu){
      if(l.getX()==posX && l.getY()==posY){
        res=l;
      }
    }
    return res;
  }

  public void jourSuivant(){
    for(Lieu l : this.lLieu){
      l.jourSuivant();
    }
  }

  public Lieu getLieuActuel(Etre e){
    // Renvoi le Lieu dans lequel se trouve l'Etre e
    Integer compteurX=0;
    Integer compteurY=0;
    while(compteurX<=this.maxX){
      while(compteurY<=this.maxY){
        Lieu lieu = this.getLieu(compteurX, compteurY);
        if(lieu != null){
          for(Etre e1 : lieu.getListHabitant()){
            if(e==e1){
              return lieu;
            }
          }
        }
        compteurY++;
      }
      compteurY=0;
      compteurX++;
    }
    return null;
  }

  public List<Etre> getListEtre(){
    // renvoi tous les Etre de tous les Lieu
    List<Etre> res = new ArrayList<>();
    for(Lieu l : this.lLieu){
      List<Etre> lEtre = l.getListHabitant();
      for( Etre e : lEtre){
        res.add(e);
      }
    }
    return res;
  }

  public List<Lieu> getLieuAdjacent(Lieu l){
    List<Lieu> lieuxAdjacent = new ArrayList<>();
    lieuxAdjacent.add(this.getLieu(l.getX()+1,l.getY()));
    lieuxAdjacent.add(this.getLieu(l.getX()-1,l.getY()));
    lieuxAdjacent.add(this.getLieu(l.getX(),l.getY()+1));
    lieuxAdjacent.add(this.getLieu(l.getX(),l.getY()-1));
    List<Lieu> res = new ArrayList<>();
    for(Lieu l1 : lieuxAdjacent){
      if(l1!=null){
        res.add(l1);
      }
    }
    return res;
  }

  public List<Lieu> getPath(Lieu l1, Lieu l2, List<Lieu> lieuxPossibles, List<Lieu> lieuxPrecedent){
    // renvoi le chemin le plus rapide en partant de l1 pour aller à l2 en tenant compte des lieux possible ( car on considerera par exemple qu'un lapin ne peut pas passer par un étant si plus tard on en a un )
    List<Lieu> res = new ArrayList<>();
    List<Lieu> lieuxAdjacent = this.getLieuAdjacent(l1);
    List<Lieu> aVisiter = new ArrayList<>();
    lieuxPrecedent.add(l1);
    if(l1.getX()==l2.getX() && l1.getY()==l2.getY()){ // on verifie le cas ou les deux lieux sont les mêmes
      return new ArrayList<Lieu>(); // et dans ce cas on ne veut pas effectuer de déplacement mais le chemin est "possible" donc on ne renvoi pas null
    }

    for(Lieu l : lieuxAdjacent){
      if(l.getX()==l2.getX() && l.getY()==l2.getY()){
        res.add(l);
        return res;
      }
      for(Lieu l3 : lieuxPossibles){
        if(l3.getClass()==l.getClass()){
          Boolean dejaVenu = false;
          for(Lieu l4 : lieuxPrecedent){
            if(l4.getX()==l.getX() && l4.getY()==l.getY()){
              dejaVenu=true;
            }
          }
          if(!dejaVenu){
            aVisiter.add(l);
          }
        }
      }
    }

    List<List<Lieu>> possibiliteChemin = new ArrayList<>();
    for(Lieu l : aVisiter){


      List<Lieu> resultat=this.getPath(l, l2, lieuxPossibles, lieuxPrecedent);
      if(resultat!=null){
        resultat.add(0, l);
        possibiliteChemin.add(resultat);
      }
    }
    if(possibiliteChemin.size()>0){
      List<Lieu> resultatPlusRapide = possibiliteChemin.get(0);
      for(List<Lieu> lLieu: possibiliteChemin){
        if(lLieu.size()<resultatPlusRapide.size()){
          resultatPlusRapide = lLieu;
        }
      }
      return resultatPlusRapide;
    }
    return null;
  }

  public List<Lieu> getCheminVersEtre(Animaux a1, Etre eArrive){ // seul un animal se déplace
    Lieu lActuel = this.getLieuActuel(a1);
    Lieu lRejoindre=this.getLieuActuel(eArrive);
    List<Lieu> path = this.getPath(lActuel, lRejoindre, a1.getDestinationPossible(), new ArrayList<Lieu>());
    return path; // Vaut null si aucun chemin n'est trouvé
  }

  public List<Lieu> getCheminVersEtrePrudent(Animaux a1, Etre eArrive){ // un animal prudent n'ira pas dans les lieux ou sont ses predateurs
    Lieu lActuel = this.getLieuActuel(a1);
    Lieu lRejoindre=this.getLieuActuel(eArrive);

    List<Lieu> aEviter = new ArrayList<>();
    for(Etre e : this.getListEtre()){
      if(e instanceof Animaux){
        Animaux a2 = (Animaux) e;
        if(a2.canEat(a1)){
          aEviter.add(this.getLieuActuel(a2));
        }
      }
    }
    List<Lieu> path = this.getPath(lActuel, lRejoindre, a1.getDestinationPossible(), aEviter);
    return path; // Vaut null si aucun chemin n'est trouvé
  }

  public List<Lieu> getCheminVersPretendant(Animaux a1){
    List<Etre> lEtre = this.getListEtre();
    Integer distance = null; // le nombre d'energie nécessaire pour rejoindre le lieu
    List<Lieu> cheminAParcourir = null;
    for(Etre e : lEtre){
      if(e.getClass() == a1.getClass()){
        Animaux a2 = (Animaux) e;
        if(a2.getMale() != a1.getMale()){ //s'il est du sexe opposé
          if(!(a2.isInCooldown())){
            List<Lieu> path = this.getCheminVersEtre(a1, a2);
            if(path!=null){
              Integer dist=path.size();
              if((a1.getEnergie()-dist)>=2){ // On ajoutera cette possibilité uniquement si le lapin aura toujours au moins 2 d'énergie à son arrivé, pour éviter les suicides
                if(distance==null){
                  distance=dist;
                  cheminAParcourir=path;
                }
                else if(dist<distance){ // et on ne l'ajoute que si elle est plus intéressante qu'avant
                  distance=dist;
                  cheminAParcourir=path;
                }
              }
            }
          }
        }
      }
    }
    return cheminAParcourir;
  }

  public List<Lieu> getCheminVersNourriture(Animaux a1){
    List<Etre> lEtre = this.getListEtre();
    Integer distance = null; // le nombre d'energie nécessaire pour rejoindre le lieu
    List<Lieu> cheminAParcourir = null;
    for(Etre e : lEtre){
      if(a1.canEat(e)){
        List<Lieu> path = this.getCheminVersEtre(a1, e);
        if(path!=null){
          Integer dist=path.size();
          if((a1.getEnergie()-dist)>0){ // On ajoutera cette possibilité uniquement si le lapin aura toujours au moins 2 d'énergie à son arrivé, pour éviter les suicides
            if(distance==null){
              distance=dist;
              cheminAParcourir=path;
            }
            else if(dist<distance){ // et on ne l'ajoute que si elle est plus intéressante qu'avant
              distance=dist;
              cheminAParcourir=path;
            }
          }
        }
      }
    }
    return cheminAParcourir;
  }





}
