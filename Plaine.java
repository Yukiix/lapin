import java.util.*;
import java.awt.Color;


public class Plaine extends Lieu{


  public Plaine(Integer posX, Integer posY){
    super(posX, posY, "Plaine");
    super.setColor(new Color(176, 227, 1));
  }
  public Plaine(){ // Une plaine est un peu comme une prairie sauf que les carottes n'y poussent pas
    super(0, 0, "Plaine");
    super.setColor(new Color(176, 227, 1));
  }

  public void jourSuivant(){
    super.jourSuivant();
  }




}
