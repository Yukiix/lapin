import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.applet.Applet;
import java.util.concurrent.TimeUnit;


public class Affichage extends Applet {
  private Monde m;
  private JButton jsuivant;
    public void init(JButton b) {
        setLayout(new GridLayout(this.m.getMaxY()+1, this.m.getMaxX()+1));
        this.majAffichage();

    }
    public Affichage(JButton b, Monde m){
      this.m=m;
      this.jsuivant=b;
      this.init(b);
    }
    public void majAffichage(){
      JButton b=this.jsuivant;
      this.removeAll();
      Integer compteurX=0;
      Integer compteurY=0;

      while(compteurX<=this.m.getMaxX()){
        while(compteurY<=this.m.getMaxY()){
          Lieu lieu = this.m.getLieu(compteurX, compteurY);
          if(lieu != null){
            JButton x = new JButton("<html><h1 style='color:black;'>"+lieu.toString()+"</h1></html>");
            x.setBackground(lieu.getColor());
            x.setEnabled(true);
            add(x);
          }
          else{
            JButton x = new JButton();
            x.setBackground(Color.black);
            x.setEnabled(false);
            add(x);
          }
          compteurY++;
        }
        compteurY=0;
        compteurX++;
      }
      add(b);
      add(new JLabel("Jour : "+this.fenetre.getJour()));
      revalidate();
      repaint();
    }

    public void majAffichageNoButton(){
      this.removeAll();
      Integer compteurX=0;
      Integer compteurY=0;
      while(compteurX<=this.m.getMaxX()){
        while(compteurY<=this.m.getMaxY()){
          Lieu lieu = this.m.getLieu(compteurX, compteurY);
          if(lieu != null){
            JButton x = new JButton("<html><h1 style='color:black;'>"+lieu.toString()+"</h1></html>");
            x.setBackground(lieu.getColor());
            x.setEnabled(true);
            add(x);
          }
          else{
            JButton x = new JButton();
            x.setBackground(Color.black);
            x.setEnabled(false);
            add(x);
          }
          compteurY++;
        }
        compteurY=0;
        compteurX++;
      }
      add(new JLabel("Attente de la fin de journée"));
      revalidate();
      repaint();


    }
}
