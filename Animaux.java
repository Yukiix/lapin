import java.util.*;
import java.lang.Math;

public abstract class Animaux extends Etre{


  private Boolean is_male;
  private Integer energie;
  private Integer seuil_affame;
  private Integer energie_max;
  private List<Etre> peut_manger;
  private Integer cooldown;
  private String nom;
  private Monde m;


  public Animaux(Monde m){
    super();
    this.m = m;
    this.cooldown = 0; // un animal ne pourra pas se reproduire tous les jours
    this.peut_manger= new ArrayList<>();
    Boolean male=false;
    Float r = (float) Math.random();

    if(r<0.5){ //Le genre du lapin est généré pseudo-aléatoirement
      male=true;
    }
    if(male){
      this.nom=this.m.getNewNomMale();
    }
    else{
      this.nom=this.m.getNewNomFemelle();
    }
    this.setMale(male);

  }

  public Monde getMonde(){
    return this.m;
  }
  public String getNom(){
    return this.nom;
  }

  public void setMale(Boolean m){
    this.is_male=m;
  }
  public Boolean getMale(){
    return this.is_male;
  }
  public void setEnergie(Integer e){
    this.energie=e;
  }
  public Integer getEnergie(){
    return this.energie;
  }
  public void addEnergie(Integer e){
    this.energie=this.energie+e;
  }
  public void setSeuilAffame(Integer s){
    this.seuil_affame=s;
  }
  public void setEnergieMax(Integer m){
    this.energie_max=m;
  }
  public Integer getEnergieMax(){
    return this.energie_max;
  }
  public void addAlimentation(Etre e){
    this.peut_manger.add(e);
  }
  public Boolean is_affame(){
    return this.energie<=this.seuil_affame;
  }
  public Boolean isInCooldown(){
    return this.cooldown>0;
  }
  public void setCooldown(Integer i){
    this.cooldown=i;
  }

  public Boolean canEat(Etre e){
    for(Etre n : this.peut_manger){
      if(n.getClass()==e.getClass()){
        if(!e.is_dead){
          return true;
        }
      }
    }
    return false;
  }

  public Boolean eat(Etre e){
    if(this.canEat(e)){
      this.energie+=e.getRapporteEnergie();
      if(this.energie > this.energie_max ){
        this.energie=this.energie_max;
      }
      e.set_dead();
      if(e instanceof Animaux){
        Animaux a = (Animaux) e;
        System.out.println(a.getNom()+" s'est fait(e) graille");
      }
      return true;
    }
    return false;
  }

  public void jourSuivant(){
    this.energie--;
    if(this.cooldown>0){
      this.cooldown--;
    }
    super.jourSuivant();
  }
  public Integer getAge(){
    return super.getAge();
  }

  public Etre changementEtat(){
    if(this.energie<=0){
      System.out.println(this.getNom()+" est mort(e) de faim rip "+this.getNom());
      return new AnimalMort(m);
    }
    return null;
  }
}
