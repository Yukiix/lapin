import java.util.*;

public abstract class Vegetaux extends Etre{


  public Vegetaux(){
    super(); // un vegetal ne se déplace pas
  }
  public void jourSuivant(){
    super.jourSuivant();
  }
  public Integer getAge(){
    return super.getAge();
  }
}
